resource "linode_sshkey" "auth_key" {
  label   = "ssh_key_si"
  ssh_key = chomp(file(var.ssh_public_key_path))
}
