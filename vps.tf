resource "linode_instance" "si-vps" {
    label = "si-test"
    image = "linode/debian10"
    region = "eu-central"
    type = "g6-nanode-1"
    authorized_keys = ["${linode_sshkey.auth_key.ssh_key}"]
    root_pass = "p@ssw0rdMariusz"

    tags = [ "spoleczenstwo-informacyjne" ]
}
